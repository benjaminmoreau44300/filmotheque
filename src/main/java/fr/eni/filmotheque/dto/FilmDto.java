package fr.eni.filmotheque.dto;

public class FilmDto {

	private Long id;
	private String nom;
	private String description;
	private String date;
	
	public FilmDto(String nom, String description, String date) {
		this.nom = nom;
		this.description = description;
		this.date = date;
	}
	
	public FilmDto() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
}
