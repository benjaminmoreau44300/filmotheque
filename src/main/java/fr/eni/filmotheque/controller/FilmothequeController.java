package fr.eni.filmotheque.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.view.RedirectView;

import fr.eni.filmotheque.dao.AvisRepository;
import fr.eni.filmotheque.dao.FilmRepository;
import fr.eni.filmotheque.dao.UtilisateurRepository;
import fr.eni.filmotheque.model.Avis;
import fr.eni.filmotheque.model.Film;
import fr.eni.filmotheque.model.ModelSession;
import fr.eni.filmotheque.model.Utilisateur;
import fr.eni.filmotheque.service.FilmService;

@Controller
@SessionAttributes({ "idSession"})
public class FilmothequeController {

	private ModelSession maSession;
	
	@Autowired
	FilmService filmService;
	
//	@Autowired
//	filmothequeRepository filmothequeRepository;
	
	@Autowired
	FilmRepository filmRepository;
	
	@Autowired
	AvisRepository avisRepository;
	
	@Autowired
	UtilisateurRepository utilisateurRepository;
	
	@Autowired
	public void setMaSession(ModelSession maSession) {
		this.maSession= maSession;
	}
	
	@GetMapping("/")
	public String filmotheque(Model modele) {
//		Utilisateur utilisateur = new Utilisateur();
//		utilisateur.setLogin("admin");
//		utilisateur.setPassword("admin");
//		utilisateurRepository.save(utilisateur);
		if(maSession.getIdSession() == null) {
			modele.addAttribute("user", new Utilisateur());
			return "login"; 	
		} else {
			List<Film> listeFilm = filmRepository.findAll();
			modele.addAttribute("listeFilm", listeFilm);
			return "index"; 			
		}
		
	}
	
	@GetMapping("/create")
	public String createForm(Model modele) {
		modele.addAttribute("film", new Film());
		if(maSession.getIdSession() == null) {
			modele.addAttribute("user", new Utilisateur());
			return "login"; 	
		} else {
			return "create"; 
		}
	}
	
	@PostMapping("/create")
	public RedirectView create(@ModelAttribute Film film, Model modele) {
		filmService.createFilm(film);
		if(maSession.getIdSession() == null) {
			modele.addAttribute("user", new Utilisateur());
			return new RedirectView("/login");
		} else {
			return new RedirectView("/");
		}
	}
	
	@GetMapping("/avis")
	public String avisForm(@RequestParam Long id, Model modele) {
		if(maSession.getIdSession() == null) {
			modele.addAttribute("user", new Utilisateur());
			return "login"; 	
		} else {
			Film film = filmRepository.findById(id).get();
			modele.addAttribute("idFilm", id);
			Avis avis = new Avis();
			modele.addAttribute("unAvis", avis);
			return "avis"; 
		}
	}
	
	@PostMapping("/avis")
	public RedirectView avis(@RequestParam String idFilm, @ModelAttribute Avis avis, Model modele) {
		if(maSession.getIdSession() == null) {
			modele.addAttribute("user", new Utilisateur());
			return new RedirectView("/login");
		} else {
			Film unFilm = filmRepository.findById(Long.parseLong(idFilm)).get();
			unFilm.getListeAvis().add(avis);
			avis.setUnFilm(unFilm);
			avisRepository.save(avis);
			filmRepository.save(unFilm);
			return new RedirectView("/");
		}
	}
	
//	@GetMapping("/delete")
//	public RedirectView delete(@RequestParam long id, Model modele) {
//		filmothequeService.deletefilmotheque(id);
//		if(maSession.getIdSession() == null) {
//			modele.addAttribute("user", new Utilisateur());
//			return new RedirectView("/login");
//		} else {
//			return new RedirectView("/");
//		}
//	}
	
	@GetMapping("/login")
	public String loginForm(@RequestParam(value = "error", defaultValue= "false") boolean loginError, Model modele) {
		   if (loginError) {
	            modele.addAttribute("error", Boolean.TRUE);
	        }
		modele.addAttribute("user", new Utilisateur());
		return "login"; 
	}
	
	@PostMapping("/login")
	public RedirectView login(@ModelAttribute Utilisateur utilisateur, Model modele) {
		Long vretour = filmService.login(utilisateur.getLogin(), utilisateur.getPassword());
		if(vretour != null && maSession.getIdSession() == null) {
			maSession.setIdSession(vretour);
			modele.addAttribute("idSession", maSession.getIdSession());
			return new RedirectView("/");
		} else {
			return new RedirectView("/login?error=true");
		}
	}
	
	@GetMapping("/logout")
	public String logout(Model modele) {
		modele.addAttribute("user", new Utilisateur());
		maSession.setIdSession(null);
		modele.addAttribute("idSession", null);
		return "login"; 
	}
	
	@GetMapping("/film")
	public String film(@RequestParam Long id, Model modele) {
		Film unFilm = filmRepository.findById(id).get();
		List<Avis> listeAvis = avisRepository.findAvisByFilmId(id).get();
		unFilm.setListeAvis(listeAvis);
		modele.addAttribute("film", unFilm);
		return "film"; 
	}
}
