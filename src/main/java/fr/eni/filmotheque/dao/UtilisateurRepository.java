package fr.eni.filmotheque.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.eni.filmotheque.model.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>{

    @Query(" select u from Utilisateur u " +
            " where u.login = ?1")
    Optional<Utilisateur> findUserWithName(String login);
}
