package fr.eni.filmotheque.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.eni.filmotheque.model.Avis;

public interface AvisRepository extends JpaRepository<Avis, Long>{

    @Query(value = "select * from avis" +
            " where un_film_id = ?1", nativeQuery = true)
    Optional<List<Avis>> findAvisByFilmId(Long id);
}
