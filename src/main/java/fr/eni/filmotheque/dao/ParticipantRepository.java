package fr.eni.filmotheque.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eni.filmotheque.model.Participant;

public interface ParticipantRepository extends JpaRepository<Participant, Long>{

}
