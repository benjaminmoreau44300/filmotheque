package fr.eni.filmotheque.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Avis {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String description;
	private String note;
	@ManyToOne
	private Utilisateur unUtilisateur;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Film unFilm;
}
