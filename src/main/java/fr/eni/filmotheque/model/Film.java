package fr.eni.filmotheque.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Film {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String titre;
	private String description;
	@OneToMany(mappedBy="unFilm", cascade = CascadeType.ALL)	
	private List<Avis> listeAvis = new ArrayList<>();
	@OneToMany(mappedBy="unFilm")	
	private List<Participant> listeParticipant = new ArrayList<>();;
	@OneToMany(mappedBy="unFilm")
	private List<Participant> unRealisateur = new ArrayList<>();;
}
