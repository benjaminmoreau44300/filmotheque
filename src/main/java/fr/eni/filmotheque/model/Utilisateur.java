package fr.eni.filmotheque.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Utilisateur {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String login;
	private String password;
	@OneToMany(mappedBy = "unUtilisateur")
	private List<Avis> listeAvis = new ArrayList<>();

	public Utilisateur(String login, String password) {
		this.login = login;
		this.password = password;
	}
	

}
