package fr.eni.filmotheque.model;

public class ModelSession {
	
	private Long idSession;

	public ModelSession() {
		this.idSession = null;
	}


	public Long getIdSession() {
		return idSession;
	}


	public void setIdSession(Long idSession) {
		this.idSession = idSession;
	}
	

}
