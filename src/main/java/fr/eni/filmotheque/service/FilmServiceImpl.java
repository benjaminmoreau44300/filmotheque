package fr.eni.filmotheque.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eni.filmotheque.dao.FilmRepository;
import fr.eni.filmotheque.dao.UtilisateurRepository;
import fr.eni.filmotheque.model.Film;
import fr.eni.filmotheque.model.Utilisateur;

@Service
public class FilmServiceImpl implements FilmService {

	@Autowired
	private FilmRepository filmRepository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Override
	public Film createFilm(Film film) {
		return filmRepository.save(film);
	}

//	@Override
//	public List<filmotheque> findAllfilmotheque() {
//		return filmothequeRepository.findAll();
//	}
//
//	@Override
//	public filmotheque updatefilmotheque(filmotheque filmothequeUpdated) {
//		filmotheque lastfilmotheque = filmothequeRepository.findById(filmothequeUpdated.getId()).get();
//		lastfilmotheque.setDate(filmothequeUpdated.getDate());
//		lastfilmotheque.setDescription(filmothequeUpdated.getDescription());
//		lastfilmotheque.setNom(filmothequeUpdated.getNom());
//		return filmothequeRepository.save(lastfilmotheque);
//	}
//
//	@Override
//	public void deletefilmotheque(Long id) {
//		filmothequeRepository.deleteById(id);
//	}

	@Override
	public Long login(String login, String password) {
		Long vretour = null;
		Utilisateur unUtilisateur = (!utilisateurRepository.findUserWithName(login).get().getLogin().isBlank()) ? utilisateurRepository.findUserWithName(login).get() : null;
		if(unUtilisateur.getPassword().equals(password)) {
		vretour = unUtilisateur.getId();
		}
		return vretour;
	}
	
}
