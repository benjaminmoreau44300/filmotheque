package fr.eni.filmotheque.service;

import fr.eni.filmotheque.model.Film;

public interface FilmService {

	Film createFilm(Film film);
//	List<filmotheque> findAllfilmotheque();
//	filmotheque updatefilmotheque(filmotheque filmotheque);
//	void deletefilmotheque(Long id);
	Long login(String login, String password);
	
}
